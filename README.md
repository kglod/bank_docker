# BANK.pl main repo

### Installation
* clone submodules: `git submodule update --init --recursive`
* run `docker-compose build`
* run `docker-compose run backend /app/manage.py migrate`
* run `docker-compose up`
* open browser `http://localhost:8000`
